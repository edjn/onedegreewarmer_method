# Europe One Degree Warmer: Methodology

[Europe One Degree Warmer](https://www.onedegreewarmer.eu/) is an analysis of analysis of temperature data from _the European Centre for Medium-Range Weather Forecasts_ for 558 European cities. The list was made up by listing the roughly 600 biggest cities (counting the population of the city proper), with around 200 cities added for being politically (e.g. Mariehamn) or geographically (e.g. Kiruna) significant. Some cities were later grouped together for their proximity (see below), landing at the final 558.

The list was compiled using [Eurostat's Urban Audit](https://ec.europa.eu/eurostat/web/regions-and-cities), [WikiData population data](https://www.wikidata.org/wiki/Wikidata:Main_Page), and [Natural Earth's Populated places dataset](https://www.naturalearthdata.com/downloads/10m-cultural-vectors/10m-populated-places/)

The data used in this is so-called “re-analysis data”, based on a variety of sources such as weather stations, weather balloons, buoys and satellite observations. Such data is well-suited to study weather patterns over periods spanning over a century, because it harmonizes inputs from thousands of data sources and makes comparisons in time and space possible. Two separate datasets have been used as the foundation for this analysis. The datasets are not directly comparable, and we have used the reconciliation algorithm outlined below to be able to make historical comparisons.

## Getting the data from ECMWF

All raw data used here [is available from an ECMWF API](https://software.ecmwf.int/wiki/display/CKB/How+to+download+ERA5+data+via+the+ECMWF+Web+API). For each month between January 1900 and December 2018, we fetched the temperature (in degrees Kelvin) for the coordinates of each city at midnight, 6 am, 12 am and 6 pm.

The resulting data is then averaged per day, converted to degrees Celsius and stored in a database.

# Merging the ECMWF data sets

The ECMWF data is actually two different data sets. One is ERA-20c, which goes from 1900 to 2010, the other is ERA-interim, from 1979 to today. ERA-interim is of much higher quality (there's not much satellite data to process in the early 20th century).

We had to modify the data from the era20c data set (1900 to 1979) so that it can be compared with the data from the interim data set (1979 &ndash;).

The difference between the datasets can be illustrated by the following code:

```py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from_year = 1979
to_year = 1997

test_city = "Islington"

city_era20c = "data/{}-era20c.csv".format(test_city)
city_interim = "data/{}-interim.csv".format(test_city)
city_era20c_df = pd.read_csv(city_era20c, index_col="Date", parse_dates=True)
city_interim_df = pd.read_csv(city_interim, index_col="Date", parse_dates=True)

city_era20c_df = city_era20c_df.loc[city_era20c_df.index.year >= from_year]
city_interim_df = city_interim_df.loc[city_interim_df.index.year <= to_year]

city_diff = city_era20c_df - city_interim_df

city_diff = city_diff.resample('D').mean()
city_diff = city_diff.interpolate()

fig, ax = plt.subplots(1, 1, figsize=(15, 5))
ax.plot(city_diff.index, city_diff["2 metre temperature"])
```
![Daily temperatures](img/1_dailytemp.png)

The difference in temperature before the two data sets is not random, it follows a seasonal pattern. This makes sense, because the squares being averaged to produce this data are not equal. The square under observation in ERA-20c might be a bit more to the South or West than the square of ERA-interim, which explains the seasonal patterns.

To account for these, we decomposed the series above to extract its seasonal component:

```py
from statsmodels.tsa.seasonal import seasonal_decompose

result = seasonal_decompose(city_diff, model='additive', freq=365)
result.plot()
plt.show()
```

![Decomposed series](img/2_decompose.png)

We then corrected the original data by removing the seasonal component from it as well as removing the average of the trend:

```py
era20c_corrected = city_era20c_df - result.seasonal - result.trend.mean()
fig, ax = plt.subplots(1, 1, figsize=(15, 5))
ax.set_xlim([dt.datetime(1979, 1, 1, 0, 0), dt.datetime(1979, 12, 31, 0, 0)])
ax.plot(era20c_corrected.index, era20c_corrected["2 metre temperature"], label="ERA 20c corrected")
ax.plot(city_era20c_df.index, city_era20c_df["2 metre temperature"], label="ERA 20c")
ax.plot(city_interim_df.index, city_interim_df["2 metre temperature"], label="ERA interim")
ax.legend(loc='upper left')
```

![Corrected series](img/3_series.png)

The correction was then applied to the data from the era20c data set from 1900 to 1979.

At this stage, we had a single data set of temperature averages by day from 1900 to 2018 for each city.
